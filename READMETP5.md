## Sécu défensive : VPN

### Installation SERVEUR

Tout d'abord, pour faire office de serveur VPN, j'ai utilisé un VPS sur le site de OVH.    
J'ai donc procédé à l'installation guidée sous Rocky 8.    

Pour commencer j'ai installé deux logiciels ainsi que wireguard :     
`sudo dnf install elrepo-release epel-release`    
`sudo dnf install kmod-wireguard wireguard-tools`    
J'ai créé une clé privée et modifié ces perms :     
`wg genkey | sudo tee /etc/wireguard/private.key        
sudo chmod go= /etc/wireguard/private.key`    
J'ai créé une clé publique grâce à la clé privée :     
`sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key`    
Ensuite jai choisi de créer l'IPV4 :     
Dans mon cas, 10.8.0.1 pour le serveur.    
J'ai créé une interface dans wireguard permettant de préciser le port d'écoute et l'IPV4 grâce à la commande :     
`sudo vi /etc/wireguard/wg0.conf`

[Interface]    
PrivateKey = aDa3bsFDH2sRC2A6l0oFoSbP2hlsOnrNHiNIFbXspW4=     
Address = 10.8.0.1/24    
ListenPort = 51820    
SaveConfig = true    

Pour faire comprendre à Wireguard qu'on utilise une IPV4 :     

`sudo vi /etc/sysctl.conf`    
et on ajoute "net.ipv4.ip_forward=1".    

On configure ensuite le PareFeu du serveur :  
On précise qu'il doit écouter sur le port 51820 en UDP  
`sudo firewall-cmd --zone=public --add-port=51820/udp --permanent`    
On ajoute l'interface wg0 à la zone "interne" :     
`sudo firewall-cmd --zone=internal --add-interface=wg0 --permanent`    

On utilise le "maquerading" sur notre IPV4 :     
`sudo firewall-cmd --zone=public --add-rich-rule='rule family=ipv4 source address=10.8.0.0/24 masquerade' --permanent`    
et ensuite on reload ce dernier :     
`sudo firewall-cmd --reload`    
Pour finir sur le PareFeu on check si les commandes qu'on a rentré sont là :     
`sudo firewall-cmd --zone=public --list-all`    
On peut ensuite lancer le serveur :     
`sudo systemctl enable wg-quick@wg0.service`    
`sudo systemctl start wg-quick@wg0.service`    
`sudo systemctl status wg-quick@wg0.service`    

### Installation CLIENT

J'ai utilisé comme client mon propre pc avec l'application (graphique) Wireguard :     
J'ai donc généré une clé privée et une clé privée, que j'ai ajouté dans l'application :     

[Interface]    
PrivateKey = Clé_privée    
Address = 10.8.0.2/24    

[Peer]    
PublicKey = Clé_publique    
AllowedIPs = 10.8.0.0/24    
Endpoint = 203.0.113.1:51820    

Après ça on active le vpn depuis l'appli, il fonctionnera sauf que quand on réalisera une recherche internet, la demande passera toujours par notre réseau et non par le réseau virtuel de notre serveur VPS.    

On doit donc modifier la route du trafic.    

Pour connaître l'ip de la passerelle par défaut :     
`ip route list table main default`    
146.59.195.81 
On cherche aussi à connaître l'IP publique :     
`ip -brief address show eth0`    
146.59.195.81 
On remarque que pour moi les IPs sont les mêmes.    
Pour finir dans l'appli, on rentre ces 4 lignes :     

PostUp = ip rule add table 200 from  ip_passerelle-defaut    
PostUp = ip route add table 200 default via ip_publique    
PreDown = ip rule delete table 200 from ip_passerelle-defaut    
PreDown = ip route delete table 200 default via ip_publique    
### VPN fonctionnel    
Notre VPN est finalement fonctionnel et la route du trafic est bien modifiée.    

Pour en être sûrs, on peut check sur notre client via quelques commandes :     
`ping 10.8.0.1` ping l'ip de la passerelle virtuelle   

Envoi d’une requête 'Ping'  10.8.0.1 avec 32 octets de données :    
Réponse de 10.8.0.1 : octets=32 temps=18 ms TTL=64    
Réponse de 10.8.0.1 : octets=32 temps=18 ms TTL=64    

 `ping 146.59.195.81` ping l'ip publique de notre serveur VPS    
 
 Envoi d’une requête 'Ping'  146.59.195.81 avec 32 octets de données :    
Réponse de 146.59.195.81 : octets=32 temps=37 ms TTL=47    
Réponse de 146.59.195.81 : octets=32 temps=25 ms TTL=47    

On check nos ips : 
`ipconfig`    
Carte inconnue VPN :    

   Suffixe DNS propre à la connexion. . . :    
   Adresse IPv4. . . . . . . . . . . . . .: 10.8.0.2    
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0    

On peut aussi check la route :     
`route print -4`    

IPv4 Table de routage :     
Itinéraires actifs :    
Destination réseau    Masque réseau  Adr. passerelle   Adr. interface Métrique    
             10.8.0.0    255.255.255.0         On-link          10.8.0.2      5
             10.8.0.2    255.255.255.255         On-link          10.8.0.2    261
             10.8.0.255  255.255.255.255       On-link          10.8.0.2    261
             
Quand on réalisera une recherche avec VPN activé la recherche proviendra de notre serveur.


